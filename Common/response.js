var moment = require('moment'); 
/**
 * sample response while operation has done
 * @param {*request} req 
 * @param {*response} res 
 * @param {*string} status 
 * @param {*array|object} response 
 */

var httpResponse = function (req, res, status, response) {
    switch (status) {
        case 'success':
            res.status(200)
                .json({
                    status: 200,
                    code: 1,
                    data: response,
                    message: "Success",
                    emptyKeys: null,
                    error: false
                })
            break;
        case 'err':
            res.status(501)
                .json({
                    status: 501,
                    code: 1,
                    data: response,
                    message: "Error",
                    emptyKeys: null,
                    error: true
                })
            break;
        case 'notValid':
            res.status(401)
                .json({
                    code: 1,
                    status: 401,
                    data: response,
                    message: "NotValid",
                    emptyKeys: null,
                    error: true
                })
            break;
        case 'emailPresent':
            res.status(409)
                .json({
                    code: 1,
                    status: 409,
                    data: response,
                    message: "emailPresent",
                    emptyKeys: null,
                    error: true
                })
            break;
        case 'phonePresent':
            res.status(409)
                .json({
                    code: 1,
                    status: 409,
                    data: response,
                    message: "phonePresent",
                    emptyKeys: null,
                    error: true
                })
            break;
        case 'noValue':
            res.status(404)
                .json({
                    code: 1,
                    status: 404,
                    data: response,
                    message: "NoValue",
                    emptyKeys: null,
                    error: true
                })
            break;
        case 'notAuthorized':
            res.status(401)
                .json({
                    code: 1,
                    status: 401,
                    data: response,
                    message: "Not Authorized",
                    emptyKeys: null,
                    error: true
                })
            break;
        case 'objEmpty':
            res.status(400)
                .json({
                    code: 1,
                    status: 400,
                    data: [],
                    message: "ObjEmpty",
                    emptyKeys: null,
                    error: true
                })
            break;
        case 'badRequest':
            res.status(400)
                .json({
                    code: 1,
                    status: 400,
                    data: response,
                    message: "Bad REQUEST",
                    emptyKeys: null,
                    error: true
                })
            break;
        case "validationErr":
            res.status(422)
                .json({
                    code: 1,
                    status: 422,
                    data: [],
                    message: "ValidationError",
                    emptyKeys: response,
                    error: true
                })
            break;
        case "verificationErr":
            res.status(304)
                .json({
                    code: 1,
                    status: 304,
                    data: [],
                    message: "VarificationError",
                    emptyKeys: response,
                    error: true
                })
            break;
        case "emailPresent":
            res.status(409)
                .json({
                    code: 1,
                    status: 409,
                    data: response,
                    message: "EmailPresent",
                    emptyKeys: [],
                    error: true
                })
            break;
        case "forbidden":
            res.status(409)
                .json({
                    code: 1,
                    status: 409,
                    data: response,
                    message: "FORBIDDEN",
                    emptyKeys: [],
                    error: true
                })
            break;
        case "logedIn":
            res.status(200)
                .json({
                    code: 1,
                    status: 200,
                    data: response,
                    message: "LogedIn",
                    emptyKeys: [],
                    error: false
                })
            break;
        case "logedOut":
            res.status(200)
                .json({
                    code: 1,
                    status: 200,
                    data: response,
                    message: "LogedOut",
                    emptyKeys: [],
                    error: false
                })
            break;
        default:
            res.status(500)
                .json({
                    code: 1,
                    status: 500,
                    data: [],
                    message: "InternalServerError",
                    emptyKeys: null,
                    error: true
                })
    }
}



/* Filter search user */
var filterSearchWith = (modalName,pageNo,pagePerItem,searchData,searchVariable,sortVariable,cb) => {
                         modalName.aggregate(
                             [
                                 { $skip: (pageNo - 1) * pagePerItem },
                                 { $limit: pagePerItem },
                                 { $sort: { [sortVariable]: -1 } },
                                 {
                                     $match: {
                                         $expr: {
                                             $ne: [{
                                                 $indexOfCP: [{ $toLower: `$${searchVariable}` }, searchData ? searchData : ""]
                                             }, -1]
                                         }
                                     }
                                 }
                             ]
                         ).then((data) => {
                             cb(true,data)
                         }).catch((err) => {
                             cb(false,err)
                         })       
 }


 /* Filter search user */
var filterSearch = (modalName,searchData,searchVariable,sortVariable,cb) => {
    console.log(startdate,enddate)
    modalName.aggregate(
        [
            { $sort: { [sortVariable]: -1 } },
            {
                $match: {
                    $expr: {
                            $ne: [{
                                $indexOfCP: [{ $toLower: `$${searchVariable}` }, searchData ? searchData : ""]
                            }, -1]                        
                    }
                }
            }
        ]
    ).then((data) => {
        cb(true,data)
    }).catch((err) => {
        cb(false,err)
    })       
}

var recurringData=(data,cb)=>{
    data['startDate']=moment(new Date(data.startDate)).format('YYYY-MM-DD, h:mm:ss a')
    var newDt = new Date(data.startDate)
    var arrayDate = [data]
    if(data.recurring === 'true'){
        for(var i=0;i<208;i++){
            var a = newDt.setDate(newDt.getDate()+7) 
            var object = {
                startDate:moment(new Date(a)).format('YYYY-MM-DD, h:mm:ss a') ,
                sprint:data.sprint,
                link:data.link,
                day:data.day,
                recurring:data.recurring
            }
            arrayDate.push(object)
        }
        cb(arrayDate)
    }else{
        cb(arrayDate)
    }
}


module.exports = {
    httpResponse,
    filterSearch,
    filterSearchWith,
    recurringData
}