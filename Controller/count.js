var validate = require('../Common/common')
var Response = require('../Common/response')
var Count = require('../Modal/count')
var mongoose = require('mongoose')
var { SUCCESS, ERROR, VALIDATE_ERROR } = require('../Common/constant')

/* Create count Api */
var createCount = (req, res) => {
    const reqBody = {
        startDate: req.body.startDate,
        sprint: req.body.sprint,
        link:req.body.link
    }
    validate.validation(Object.keys(reqBody), reqBody)
        .then(({ status, response }) => {
            if (status) {
               var count = new Count(reqBody)
                count.save()
                .then((data)=>{
                    Response.httpResponse(req, res, SUCCESS, data)
                }).catch((err)=> Response.httpResponse(req, res, ERROR, err))
            } else {
                Response.httpResponse(req, res, VALIDATE_ERROR, response)
            }
        }).catch(err => Response.httpResponse(req, res, VALIDATE_ERROR, err));
}


/* Get count Api with search */
var getCount = (req, res) => {
    const reqBody = {
        pageNo: parseInt(req.body.pageNo) ,
        pagePerItem: parseInt(req.body.pagePerItem)
    }
    validate.validation(Object.keys(reqBody), reqBody)
        .then(({ status, response }) => {
            if (status) {
                var searchDate = req.body.searchDate ? req.body.searchDate : ""
                Response.filterSearchWith(Count,reqBody.pageNo,reqBody.pagePerItem,searchDate,"startDate","startDate",(status,result)=>{
                    if(status){
                            Response.httpResponse(req, res, SUCCESS, result)
                    }else{
                        Response.httpResponse(req, res, VALIDATE_ERROR, result)
                    }
                })
            //    Count.find({startDate:reqBody.searchDate}).limit(reqBody.pagePerItem).skip(skip)
            //     .then((data)=>{
            //         Response.httpResponse(req, res, SUCCESS, data)
            //     }).catch((err)=> Response.httpResponse(req, res, ERROR, err))
            } else {
                Response.httpResponse(req, res, VALIDATE_ERROR, response)
            }
        }).catch(err => Response.httpResponse(req, res, VALIDATE_ERROR, err));
}

/**Get all record */
var getCountAll = (req, res) => {
var reqBody={
    pageNo:parseInt(req.query.pageNumber),
    pagePerItem:parseInt(req.query.pageSize),
    searchDate:req.query.search
}
console.log(reqBody)
    Response.filterSearch(Count,reqBody.searchDate,"startDate","startDate",(status,data)=>{
        if(status){
                Response.httpResponse(req, res, SUCCESS, data)
        }else{
            Response.httpResponse(req, res, VALIDATE_ERROR, data)
        }
    })
}

/* update count Api */
var updateCount = (req, res) => {
    const reqBody = {
        id:req.body.id,
        startDate: req.body.startDate,
        sprint: req.body.sprint,
        link:req.body.link
    }
    validate.validation(Object.keys(reqBody), reqBody)
        .then(({ status, response }) => {
            if (status) {
               var update={
                startDate: req.body.startDate,
                sprint: req.body.sprint
               }
                Count.update({_id:req.body.id},{$set:update},function(err,data){
                    if(err){
                        Response.httpResponse(req, res, ERROR, err)
                    }else{
                        Count.find({_id:req.body.id}).then((result)=>{
                            Response.httpResponse(req, res, SUCCESS, result)
                        }).catch(error=> Response.httpResponse(req, res, ERROR, err))  
                    }
                })
            } else {
                Response.httpResponse(req, res, VALIDATE_ERROR, response)
            }
        }).catch(err => Response.httpResponse(req, res, VALIDATE_ERROR, err));
}

/* Delete count Api */
var deleteCount = (req, res) => {
    const reqBody = {
        id:req.body.id
    }
    validate.validation(Object.keys(reqBody), reqBody)
        .then(({ status, response }) => {
            if (status) {
                Count.deleteOne({_id:reqBody.id})
                .then((data)=>{
                    Response.httpResponse(req, res, SUCCESS, data)
                }).catch((err)=> Response.httpResponse(req, res, ERROR, err))
            } else {
                Response.httpResponse(req, res, VALIDATE_ERROR, response)
            }
        }).catch(err => Response.httpResponse(req, res, VALIDATE_ERROR, err));
}

module.exports = {
    createCount,
    getCount,
    getCountAll,
    updateCount,
    deleteCount
}