var validate = require('../Common/common')
var Response = require('../Common/response')
var Recurring = require('../Modal/recurring')
var mongoose = require('mongoose')
var { SUCCESS, ERROR, VALIDATE_ERROR } = require('../Common/constant')

/* Create count Api */
var createRecurring = (req, res) => {
    const reqBody = {
        startDate: req.body.startDate,
        sprint: req.body.sprint,
        link:req.body.link,
        day:req.body.day,
        recurring:req.body.recurring

    }
    // Response.recurringData(reqBody,(result)=>{
    //     res.json(result)
    // })

    validate.validation(Object.keys(reqBody), reqBody)
        .then(({ status, response }) => {
            if (status) {
                Response.recurringData(reqBody,(result)=>{
                    Recurring.collection.insert(result, function (err, data) {
                        if (err){ 
                             console.log(err);
                             Response.httpResponse(req, res, ERROR, err)
                        } else {
                          console.log("Multiple documents inserted to Collection");
                          Response.httpResponse(req, res, SUCCESS, data.ops)
                        }
                      });
                })
            } else {
                Response.httpResponse(req, res, VALIDATE_ERROR, response)
            }
        }).catch(err => Response.httpResponse(req, res, VALIDATE_ERROR, err));
}

/**Get data between the date */
var getRecurring = (req, res) => {
    const reqBody = {
        startdate: req.body.startDate,
        enddate: req.body.endDate
    }
    validate.validation(Object.keys(reqBody), reqBody)
        .then(({ status, response }) => {
            var findData = {startDate:{$gte:reqBody.startdate,$lte:reqBody.enddate}}
            if (status) {
                Recurring.find(findData).then((data)=>{
                    Response.httpResponse(req, res, SUCCESS, data)
                }).catch((err)=>Response.httpResponse(req, res, VALIDATE_ERROR, err))
            } else {
                Response.httpResponse(req, res, VALIDATE_ERROR, response)
            }
        }).catch(err => Response.httpResponse(req, res, VALIDATE_ERROR, err));
}


/* update count Api */
var updateRecurring = (req, res) => {
    const reqBody = {
        id:req.body.id,
        startDate: req.body.startDate,
        sprint: req.body.sprint,
        link:req.body.link,
        day:req.body.day,
        recurring:req.body.recurring
    }
    validate.validation(Object.keys(reqBody), reqBody)
        .then(({ status, response }) => {
            if (status) {
               var update={
                startDate: req.body.startDate,
                sprint: req.body.sprint,
                link:req.body.link,
                day:req.body.day,
                recurring:req.body.recurring
               }
               Recurring.update({_id:req.body.id},{$set:update},function(err,data){
                    if(err){
                        Response.httpResponse(req, res, ERROR, err)
                    }else{
                        Recurring.find({_id:req.body.id}).then((result)=>{
                            Response.httpResponse(req, res, SUCCESS, result)
                        }).catch(error=> Response.httpResponse(req, res, ERROR, err))  
                    }
                })
            } else {
                Response.httpResponse(req, res, VALIDATE_ERROR, response)
            }
        }).catch(err => Response.httpResponse(req, res, VALIDATE_ERROR, err));
}

/* Delete count Api */
var deleteRecurring = (req, res) => {
    const reqBody = {
        id:req.body.id
    }
    validate.validation(Object.keys(reqBody), reqBody)
        .then(({ status, response }) => {
            if (status) {
                Recurring.deleteOne({_id:reqBody.id})
                .then((data)=>{
                    Response.httpResponse(req, res, SUCCESS, data)
                }).catch((err)=> Response.httpResponse(req, res, ERROR, err))
            } else {
                Response.httpResponse(req, res, VALIDATE_ERROR, response)
            }
        }).catch(err => Response.httpResponse(req, res, VALIDATE_ERROR, err));
}

module.exports = {
    createRecurring,
    updateRecurring,
    deleteRecurring,
    getRecurring
}