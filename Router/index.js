var express = require('express');
var router = express.Router();
var count = require('./count');
var recurring = require('./recurring')
var validate = require('../Common/common')
var Response = require('../Common/response')
var { SUCCESS, ERROR, VALIDATE_ERROR, NOT_VALID, NOVALUE, ERROR } = require('../Common/constant')


/*All count route */
router.use('/count', count);
router.use('/recurring',recurring)

router.post('/login',(req,res)=>{
    var reqBody = {
        email:req.body.email,
        password:req.body.password
    }
    validate.validation(Object.keys(reqBody), reqBody)
    .then(({ status, response }) => {
        if(status){
            if(reqBody.email === "test@gmail.com" && reqBody.password === "123456"){
                Response.httpResponse(req, res, SUCCESS, "User Login Successfully")
            }else{
                Response.httpResponse(req, res, ERROR, "Email or password either wrong")
            }
        }else{
            Response.httpResponse(req, res, VALIDATE_ERROR, response)
        }
    })
    .catch(err => Response.httpResponse(req, res, VALIDATE_ERROR, err));

})


module.exports = router