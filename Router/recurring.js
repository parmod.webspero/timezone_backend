var express = require('express');
var router = express.Router();
var recurring = require('../Controller/recurring')


/*All count route */
router.post('/create', recurring.createRecurring);
router.post('/update', recurring.updateRecurring);
router.post('/get', recurring.getRecurring);
router.post('/delete', recurring.deleteRecurring);

module.exports = router