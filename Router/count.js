var express = require('express');
var router = express.Router();
var count = require('../Controller/count')


/*All count route */
router.post('/create', count.createCount);
router.post('/update', count.updateCount);
router.post('/get', count.getCount);
router.get('/get', count.getCountAll);
router.post('/delete', count.deleteCount);

module.exports = router