const mongoose = require('mongoose');

const recurringSchema = mongoose.Schema({
    startDate:{
        type:String,
        required:true
    },
    sprint:{
        type:String,
        required:true
    },
    day:{
        type:String,
        required:true 
    },
    recurring:{
        type:Boolean,
        required:true
    },
    link:{
        type:String,
        required:true
    }
}, {
    timestamps: true
})
module.exports = mongoose.model('Recurring',recurringSchema)
