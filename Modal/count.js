const mongoose = require('mongoose');

const countSchema = mongoose.Schema({
    startDate:{
        type:String,
        required:true
    },
    sprint:{
        type:String,
        required:true
    },
    link:{
        type:String,
        required:true
    }
}, {
    timestamps: true
})
module.exports = mongoose.model('Counts',countSchema)
