
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

// const url = 'mongodb://localhost:27017/Coundown'

const url = "mongodb+srv://caveday:c@v3d@y123@caveday.vzqe4.mongodb.net/caveday?retryWrites=true&w=majority";

// Connecting to the database
mongoose.connect(url, {
    useNewUrlParser: true
}).then(() => {
    console.log("Successfully connected to the database");    
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
});