module.exports = {
    apps : [{
      name: 'CounDown-Timer',
      script: 'bin/www.js',
   
      // Options reference: https://pm2.keymetrics.io/docs/usage/application-declaration/
      args: 'one two',
      instances: 'max',
      autorestart: true,
      ignore_watch : ["node_modules"],
      watch: true,
      max_memory_restart: '1G',
      env: {
        NODE_ENV: 'development'
      },
      env_production: {
        NODE_ENV: 'production'
      }
    }],
   
    deploy : {
      production : {
        'ref'  : 'origin/master',
        'repo' : 'git@github.com:repo.git',
        'path' : '/var/www/Coundown_backend',
        'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env production'
      }
    }
   };